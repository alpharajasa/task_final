package com.example.task_final

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import com.example.task_final.databinding.ActivitySopiLayoutBinding

class SopiLayout : AppCompatActivity() {

    private lateinit var binding: ActivitySopiLayoutBinding
    private lateinit var adapter: imageSliderAdapter
    private val list = ArrayList<imageData>()
    private lateinit var dots : ArrayList<TextView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySopiLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        list.add(
            imageData(
                R.drawable.shopeebanner
            )
        )
        list.add(
            imageData(
                R.drawable.shopeebanner1
            )
        )
        list.add(
            imageData(
                R.drawable.shopeebanner2
            )
        )
        list.add(
            imageData(
                R.drawable.shopeebanner3
            )
        )
        adapter = imageSliderAdapter(list)
        binding.viewPager.adapter = adapter
        dots = ArrayList()
        setIndicator()

        binding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                selectedDot(position)
                super.onPageSelected(position)
            }
        })

    }

    private fun selectedDot(position: Int) {
        for (i in 0 until list.size){
            if (i == position)
                //com.google.android.material.R.color.material_grey_600
                dots[i].setTextColor(ContextCompat.getColor(this, com.google.android.material.R.color.material_grey_600))
            else
                //com.google.android.material.R.color.material_grey_300
                dots[i].setTextColor(ContextCompat.getColor(this, com.google.android.material.R.color.material_grey_300))
        }

    }

    private fun setIndicator() {
        for (i in 0 until list.size){
            dots.add(TextView(this))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                dots[i].text=Html.fromHtml("&#9679",Html.FROM_HTML_MODE_LEGACY).toString()
            } else {
                dots[i].text=Html.fromHtml("&#9679")
            }
            dots[i].textSize = 18f
            binding.dotsIndikator.addView(dots[i])
        }
    }
}
