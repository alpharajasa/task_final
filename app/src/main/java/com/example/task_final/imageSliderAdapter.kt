package com.example.task_final

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.task_final.databinding.SliderItemBinding

class imageSliderAdapter(private val items: List<imageData>) : RecyclerView.Adapter<imageSliderAdapter.ImageViewHolder>() {

    inner class ImageViewHolder(itemView: SliderItemBinding) : RecyclerView.ViewHolder(itemView.root){
        private val binding = itemView
        fun bind(data:imageData){
            with(binding){
                Glide.with(itemView)
                    .load((data.imageDrawable))
                    .into(ivSlider)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return  ImageViewHolder(SliderItemBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(items[position])

    }

    override fun getItemCount(): Int = items.size

}