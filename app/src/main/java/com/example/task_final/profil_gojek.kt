package com.example.task_final

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout

class profil_gojek : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profil_gojek)


        val btn = findViewById<ImageView>(R.id.backarrow)
        btn.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View): Unit {
                val intent = Intent(this@profil_gojek, MainActivity::class.java);
                startActivity(intent);
            }
        })

        val btn2 = findViewById<LinearLayout>(R.id.tombolsopi)
        btn2.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View): Unit {
                val intent = Intent(this@profil_gojek, SopiLayout::class.java);
                startActivity(intent);
            }
        })
    }
}